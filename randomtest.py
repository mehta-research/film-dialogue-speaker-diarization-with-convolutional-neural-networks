import random

size = 10
numbers = list(range(size))

'''
first find the total number of batches
calculated as: take a speaker s, get all their utterances U. Total comparisons for that speaker is |U|^2 / 2. Add up all of the total comparisons. Now divide this by batch size/2. Batch size/2 because half of each batch is same
'''
