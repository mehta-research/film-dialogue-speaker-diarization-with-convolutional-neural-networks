FROM tensorflow/tensorflow:latest-py3

RUN apt -y update && apt -y install libsndfile1 ffmpeg
RUN pip3 install librosa
RUN pip3 install matplotlib

