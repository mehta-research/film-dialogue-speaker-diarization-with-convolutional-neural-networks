import wave
import numpy
import math
import random
import librosa
import soundfile
import os
import bisect

def factor(n, startFrom=2):
	"""returns a list of prime factors of n,
	knowing min possible >= startFrom."""
	if n <= 1:	return [ ]
	d = startFrom
	factors = [ ]
	while n >= d*d:
		if n % d == 0:
			factors += [d]
			n = n//d
		else:
			d += 1 + d % 2	# 2 -> 3, odd -> odd + 2
	factors += [n]
	return factors

'''
Our class for managing batches.
We apply affine transformations (e.g. overlaying music, noise, etc) to each of the samples to create more realistic training data
'''
class BatchManager:
	def __init__(self, batch_size, max_batches=-1, utterances_length=2, nth_batch=0, wav_root_dir="voxceleb/vox1/vox1_dev/vox1_dev_wav/wav", spectrogram_root_dir="voxceleb/vox1/vox1_dev/vox1_dev_spectrogram/spectrogram"):
		self.batch_size = batch_size
		self.wav_root_dir = wav_root_dir
		self.spectrogram_root_dir = spectrogram_root_dir
		self.speakers = os.listdir(self.spectrogram_root_dir)
		self.utterances_length = utterances_length
		
		#### private variables
		self.utterances_cache = {}
		
		# get the max batches and calculate the speaker ranges
		self.same_sample_ranges = []
		self.utterances_ranges = []
		self.different_sample_row_array = [] #computing this is costly, so only do it once
		max_possible_batches = self._calculate_max_batches()
		
		'''
		if(max_batches == -1):
			self.max_batches = max_possible_batches		
		else:
			self.max_batches = max_batches
		'''
		
		self.max_batches = max_possible_batches

		#in order to get batches with some semblance of randomness, we use a mapping in the form of (ax)mod m
		#m is our period of our map, and in this case it's going to be max_batches * batch_size, and a should be coprime to m
		
		#first we get m
		self.m = self.max_batches * self.batch_size
		#now we get a, factoring it, and creating a new number that has factors that are distinctly not those of m
		coprime_factors = []
                m_factors = factor(self.m)
		for f in m_factors:
			if(f == 2):
				coprime_factors += [3]
				continue
			f += 2
			factors = factor(f)
                        #we have the make sure f is a prime and that it is not a factor of m, so we just incrememt f until this is true.
			while(len(factors) >= 2 and f not in m_factors):
				f += 2
				factors = factor(f)
			coprime_factors += [f]
		self.a = 1
		for f in coprime_factors:
			self.a *= f
		
		#store the idx of the last batch we were on
		self.last_batch = nth_batch*batch_size

	def _get_all_utterances(self, speaker):
		if(speaker in self.utterances_cache):
			return self.utterances_cache[speaker]
		
		utterances = []
		for d in os.listdir(self.spectrogram_root_dir + "/" + speaker):
			for w in os.listdir(self.spectrogram_root_dir + "/" + speaker + "/" + d):
				for u in os.listdir(self.spectrogram_root_dir + "/" + speaker + "/" + d + "/" + w):
					utterances += [self.spectrogram_root_dir + "/" + speaker + "/" + d + "/" + w + "/" + u]
				'''
				f = wave.open(self.wav_root_dir + "/" + speaker + "/" + d + "/" + w, "rb")
				
				if(f.getnframes()/f.getframerate() >= self.utterances_length):
					for u in range((f.getnframes()//f.getframerate())//self.utterances_length):
						utterances += [(self.wav_root_dir + "/" + speaker + "/" + d + "/" + w, u)]
				f.close()
				'''	
		self.utterances_cache[speaker] = utterances
		return self.utterances_cache[speaker]
	
	'''
	The max batches is a function of the number of like comparisons, because there will necessarily be
	less like comparisons than different comparisons
	Since we use the number of utterances here anyway, we can also populate the utterances ranges
	'''
	def _calculate_max_batches(self):
		total_same_comparisons = 0
		self.same_sample_ranges = [0]
		self.utterances_ranges = [0]
		self.different_sample_row_array = [0]
		for s, speaker in enumerate(self.speakers):
			num_utterances = len(self._get_all_utterances(speaker))
			#first populate same sample ranges, what we came here to do
			total_same_comparisons += (num_utterances**2-num_utterances)//2
			self.same_sample_ranges += [total_same_comparisons]
			#but also do the utterances ranges because we can
			self.utterances_ranges += [self.utterances_ranges[-1] + num_utterances]
			#finally do the different sample row array because its expensive and we only need it once
			#we must do this because there are only "# of speakers" - 1 good rows.
			if(s < len(self.speakers)-1):
				num_utterances_sp1 = len(self._get_all_utterances(self.speakers[s+1]))
				self.different_sample_row_array += [self.different_sample_row_array[-1] + num_utterances_sp1*self.utterances_ranges[-1]]

		total_same_comparisons //= self.batch_size
		return total_same_comparisons
	
	'''
	Determine which speaker category this number in "# of same samples" space belongs to
	We can convieniently use bisect_left
	'''
	def _find_same_sample_speaker_category(self, number):
		if(number >= self.same_sample_ranges[-1]):
			return len(self.same_sample_ranges)-2
		elif(number < 0):
			return 0
		return bisect.bisect(self.same_sample_ranges, number)-1

	'''
	A very convienient class for finding getting the item that would be at the index in the "row array".
	Saves memory as the indices are computed on the fly
	'''
	class same_sample_row_array:
		def __init__(self, num_rows):
			self.num_rows = num_rows
		def __getitem__(self, key):
			return key*(key+1)//2
		def __len__(self):
			return self.num_rows + 1
		
	'''
	We map an integer from "# of same samples" space to "same sample pairs" space
	We do this by first finding which speaker category our number falls in; i.e., which indices is our number between iun same_sample_ranges
	We then get the invidual "pair number"; we can think of each speaker same-pairs as a stair case like:
	  0 1 2 3 4
	0 _
	1|_|_
	2|_|_|_
	3|_|_|_|_
	4|_|_|_|_|
	  0 1 2 3 4

	In this picture, the number of rows/columns represents the total number of samples in the speaker space. In this example, the number of samples
	is 5. This means that the number of possible comparisons (without comparing a sample to itself) is (5^2-5)/2. The 5^2 is the total number of 
	comparisons. The -5 is the diagonal, where we compare a sample to itself. The /2 is because half of the remaining samples are duplicates.
	Now that we have this stair case, we notice that the total number of samples is also 1 + 2 + 3 + 4; This gives us a similar problem to the 
	first step of finding the speaker category of a number; we can imagine that we have an array like [0,1,3,6,10]. If our number is < 1, we know
	that our sample is in the first row. If our number is >= 1 but < 3, then our sample must lie in the second row, if our number is >= 3 but < 6 it
	lies in the 3rd row and etc. Then if we find the row, we can easily find the collumn by taking our number and subtracting the lower bound. Example:
	Number is 2. 1 <= 2 < 3. 2 is in the second row. Now we do 2-1 to find out it's in the 1'st colum on the second row.
	Luckily, the python bisect module has this behavior
	'''
	def _int_to_same_sample_pair(self, number):
		#first get the speaker we belong to
		speaker_cat = self._find_same_sample_speaker_category(number)
		#now get our speaker relative sample number
		number -= self.same_sample_ranges[speaker_cat]
		#now get the row from our row array class
		row_array = self.same_sample_row_array(len(self._get_all_utterances(self.speakers[speaker_cat])))
		row = bisect.bisect(row_array, number)
		#subtract the lower bound from our number
		#lower bound is always row-1
		number -= row_array[row-1]
		#now we have our speaker and our pair
		return (self._get_all_utterances(self.speakers[speaker_cat])[row], self._get_all_utterances(self.speakers[speaker_cat])[number])
	
	'''
	When we do this mapping, we are taking a number from "# of different samples" space to "different sample pairs" space
	We do this by first finding which two speaker categories each number belongs to, and then we use some modulo to determine the exact row/collumn within those speaker categories
	Similar to before, comparing the speakers and selecting which pairs of speakers are possible forms a kind of stair case:
	  0 1  2   3
	  __________
	0|_|__|___|_|
	1|x|  |   | |
	 |_|__|___|_|
	2|x|xx|   | |
	 |x|xx|   | |
	 |_|__|___|_|
	3|x|xx|xxx|_|
	
	The pairs of categories we wish to select are denoted with x's. Notice that (0,0) is not a pair we want, because any sample from (0,0) actually belongs to the same sample space. 
	In this picture, we see that each category may be "longer" or "shorter" than the others. This correspons to the amount of utterances in that category. In order to map a number to a
	pair of categories, we will use a similar method to mapping a number to a pair of utterances from the same-sample space. We notice that the total number of samples in the 1st row is
	x1(x0), and for the second x2(x0+x1), and for the 3rd is x3(x0+x1+x2), and we should see a pattern here. So we can easily determine the row first. We can create an array like
	[0, x1(x0), x1(x0) + x2(x0+x1), x1(x0) + x2(x0+x1) + x3(x0+x1+x2)] and use bisect to find the row. Then once we have the row, we can subtract the lower bound from our number, and 
	use bisect again, this time on an array like [0, xk(x0), xk(x0+x1), ..., xk(x0+...+xk-1)]. We see that we can divide our number by xk as well as the array and we will get the same result.
	This means that we can actually bisect on [0, x0, x0+x1, x0+x1+x2,...] which we will have already computed
	'''
	def _int_to_dif_sample_pair(self, number):
		#first we do a bisect to determine the row
		row = bisect.bisect(self.different_sample_row_array, number)
		num_utterances_row = len(self._get_all_utterances(self.speakers[row]))
		#subtract to lower bound
		number -= self.different_sample_row_array[row-1]
		#subtract 1 here because bisect will return the pos of the upper bound; we want the lower bound
		column = bisect.bisect(self.utterances_ranges, number/num_utterances_row)-1
		#subtract the lower bound * # utterances to get the number in speaker1xspeaker2 space
		number -= self.utterances_ranges[column]*num_utterances_row
		#now we can do some modulo/division to get the actual sample numbers!
		#s1 = speaker 1
		num_utterances_s1 = num_utterances_row 
		num_utterances_s2 = len(self._get_all_utterances(self.speakers[column]))
		space = num_utterances_s1*num_utterances_s2
		return (self._get_all_utterances(self.speakers[row])[number//num_utterances_s2], 
				self._get_all_utterances(self.speakers[column])[number%num_utterances_s2])
	
	def _psuedorandom_map(self, number):
		return (self.a*number)%self.m
	
	'''
	We get batches by iterating linearly and then using our modulus map to semi randomize the samples we choose
	'''
	def get_next_batch(self):	
		
		batch_pre = []

		#half a batch size because half the batch is like examples and half is different exmaples
		for i in range(self.last_batch, self.last_batch+self.batch_size//2):
			number = self._psuedorandom_map(i)
			#the like example
			batch_pre += [(self._int_to_same_sample_pair(number), 0)]

			#map our number from the smaller max_batches*self.batch_size space to the large dif_sample_row_array[-1] space, and ignore the lack of ontoness
			batch_pre += [(self._int_to_dif_sample_pair(int(number*self.different_sample_row_array[-1]/(self.max_batches*self.batch_size))), 1)]
		
		
		#load in the waveforms and convert to spectrograms
		spects = [None]*self.batch_size
	
		for i in range(self.batch_size):
			S1 = numpy.load(batch_pre[i][0][0])
			S2 = numpy.load(batch_pre[i][0][1])
			'''
			#load in the file
			d, sr = soundfile.read(batch_pre[i][0][0][0])
			
			#determine which part it it is	
			offset = sr*batch_pre[i][0][0][1]
			length = sr*self.utterances_length
			d = d[offset:offset+length]
	
			#generate the spectrogram
			S1 = librosa.feature.melspectrogram(y=d, sr=sr, n_mels=128, n_fft=1024, hop_length=128)
			S1 = librosa.power_to_db(S1, ref=numpy.max)
			
			#load in the file
			d, sr = soundfile.read(batch_pre[i][0][1][0])
	
			#determine which part it it is	
			offset = sr*batch_pre[i][0][1][1]
			length = sr*self.utterances_length
			d = d[offset:offset+length]
	
			#generate the spectrogram
			S2 = librosa.feature.melspectrogram(y=d, sr=sr, n_mels=128, n_fft=1024, hop_length=128)	
			S2 = librosa.power_to_db(S2, ref=numpy.max)
			'''
			spects[i] = (S1, S2)
			

		h1 = spects[0][0].shape[0]
		h2 = spects[0][1].shape[0]
		w1 = spects[0][0].shape[1]
		w2 = spects[0][1].shape[1]
		
		batch = [
			[numpy.zeros((self.batch_size, h1, w1, 1)), numpy.zeros((self.batch_size, h2, w2, 1))], 
			numpy.zeros((self.batch_size))
			]
		
		for i in range(self.batch_size):
			batch[0][0][i:i+1,:h1,:min(spects[i][0].shape[1], w1),0] = spects[i][0][...,:min(w1, spects[i][0].shape[1])] #np array of first inputs
			batch[0][1][i:i+1,:h2,:min(spects[i][1].shape[1], w2),0] = spects[i][1][...,:min(w2, spects[i][1].shape[1])] #np array of first inputs
			batch[1][i] = batch_pre[i][1]
		
		
		self.last_batch += self.batch_size//2
		

		return batch
'''
This is the class we use to get our next epoch. Epochs are not the entire dataset. Each epoch is roughly 1/100th of the dataset.
'''
class EpochManager:
	'''
	epoch_size is the fraction of the total dataset that one epoch is equal to.
	validation_size is the fraction of the epoch_size that the validation set is equal to
	'''
	def __init__(self, epoch_size=0.01, validation_size=0.2, batch_size=64):
		self.batch_size = batch_size
		self.batch_manager = BatchManager(batch_size)
		self.epoch_size = int(self.batch_manager.max_batches*epoch_size)
		self.validation_size = int(validation_size*self.epoch_size) 
		self.training_size = self.epoch_size - self.validation_size
		self.epoch_iter = 0

	#we want to return two batch managers: one for the training set and another for the validation set
	def get_next_epoch(self):
		training_set = BatchManager(self.batch_size, nth_batch=self.epoch_iter*self.epoch_size)
		validation_set = BatchManager(self.batch_size, nth_batch=self.epoch_iter*self.epoch_size + self.training_size)
		self.epoch_iter += 1
		return [training_size, validation_set]

#ignore, code for testing purposes
'''
batch_manager = BatchManager(1000)

for i in range(1000):
	b = batch_manager.get_next_batch()
	print(len(b[0][0]))
'''
