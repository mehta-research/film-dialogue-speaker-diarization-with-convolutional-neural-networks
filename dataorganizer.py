import wave
import numpy
import math
import random
import librosa
import soundfile
import os
import bisect

wav_root_dir = "voxceleb/vox1/vox1_dev/vox1_dev_wav/wav"
utterance_length = 2

spectrogram_root_dir = "voxceleb/vox1/vox1_dev/vox1_dev_spectrogram/spectrogram"

'''
we are going to iterate through all the utterances and convert each one to a spectrogram
we use numpy.save to store the specrogram
'''

speakers = os.listdir(wav_root_dir)

for i, speaker in enumerate(speakers):
	print(str(i) + "/" + str(len(speakers)) + "("+str(i/len(speakers)*100)+"%)")
	os.makedirs(spectrogram_root_dir + "/" + speaker, exist_ok=True)
	for d in os.listdir(wav_root_dir + "/" + speaker):
		os.makedirs(spectrogram_root_dir + "/" + speaker + "/" + d, exist_ok=True)
		for w in os.listdir(wav_root_dir + "/" + speaker + "/" + d):
			data, sr = soundfile.read(wav_root_dir + "/" + speaker + "/" + d + "/" + w)
			if(len(data)/sr >= utterance_length):
				for u in range((len(data)//sr)//utterance_length):
					offset = sr*u
					length = sr*utterance_length
					utt = data[offset:offset+length]
					S = librosa.feature.melspectrogram(y=utt, sr=sr, n_mels=128, n_fft=1024, hop_length=128)
					S = librosa.power_to_db(S, ref=numpy.max)
					os.makedirs(spectrogram_root_dir + "/" + speaker + "/" + d + "/" + w, exist_ok=True)
					numpy.save(spectrogram_root_dir + "/" + speaker + "/" + d + "/" + w + "/" + str(u), S)
