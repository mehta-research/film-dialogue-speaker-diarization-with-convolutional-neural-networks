from __future__ import absolute_import, division, print_function, unicode_literals
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import os

from tensorflow.python.framework import dtypes
from tensorflow.python.framework import tensor_shape
from tensorflow.python.layers import base
from tensorflow.python.layers import utils
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import nn

import tensorflow as tf
import tensorflow.keras.backend as K

from tensorflow.keras.layers import Layer, Conv2D, MaxPool2D, Flatten, Dense, Input, Subtract, Lambda
from tensorflow.keras import Model, Sequential
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import Adam, SGD

from dataloader import BatchManager

#config = tf.ConfigProto()
#config.gpu_options.allow_growth = True
#sess = tf.Session(config=config)

os.environ["CUDA_VISIBLE_DEVICES"]="0,1,2,3,4,5,6,7"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '0'

print("Physical devices found by Tensorflow: ")
print(len(tf.config.experimental.list_physical_devices('GPU')))

mirrored_strategy = tf.distribute.MirroredStrategy()

#build the model

with mirrored_strategy.scope():
	convolutional_net = Sequential()
	
	print("Created sequential model")
	
	convolutional_net.add(Conv2D(filters=64, kernel_size=(3, 3),
		 activation='relu',
		 #input_shape=self.input_shape,
		 kernel_regularizer=l2(),
		 name='Conv1'))
	
	print("Added conv2d")
	
	convolutional_net.add(MaxPool2D(data_format="channels_first"))
	
	print("Added maxpool2d")
	
	convolutional_net.add(Conv2D(filters=64, kernel_size=(3, 3),
		 activation='relu',
		 kernel_regularizer=l2(),
		 name='Conv2'))
	
	print("Added conv2d")
	
	convolutional_net.add(MaxPool2D(data_format="channels_first"))
	
	print("Added maxpool2d")
	
	convolutional_net.add(Conv2D(filters=64, kernel_size=(4, 4),
		 activation='relu',
		 kernel_regularizer=l2(),
		 name='Conv3'))
	
	print("Added conv2d")
	
	convolutional_net.add(MaxPool2D(data_format="channels_first"))
	
	print("Added maxpool2d")
	
	convolutional_net.add(Conv2D(filters=64, kernel_size=(4, 4),
		 activation='relu',
		 kernel_regularizer=l2(),
		 name='Conv4'))
	
	print("Added conv2d")
	
	convolutional_net.add(MaxPool2D(data_format="channels_first"))
	
	print("Added maxpool2d")
	
	convolutional_net.add(Conv2D(filters=64, kernel_size=(4, 4),
		 activation='relu',
		 kernel_regularizer=l2(),
		 name='Conv5'))
	
	print("Added conv2d")
	
	convolutional_net.add(MaxPool2D(data_format="channels_first"))
	
	print("Added maxpool2d")
	
	convolutional_net.add(Conv2D(filters=64, kernel_size=(4, 4),
		 activation='relu',
		 kernel_regularizer=l2(),
		 name='Conv6'))
	
	print("Added conv2d")
	
	convolutional_net.add(MaxPool2D(data_format="channels_first"))
	
	print("Added maxpool2d")
	
	
	convolutional_net.add(Flatten())
	
	print("Added flatten")
	
	convolutional_net.add(Dense(units=4096,
		activation='relu',
		kernel_regularizer=l2(),
		name='Dense1'))
	
	print("Added dense")
	
	# Now the pairs of images
	input_image_1 = Input(shape=(128,251,1))
	input_image_2 = Input(shape=(128,251,1))
	
	encoded_image_1 = convolutional_net(input_image_1)
	encoded_image_2 = convolutional_net(input_image_2)
	
	print("Created twins")
	
	# L1 distance layer between the two encoded outputs
	# One could use Subtract from Keras, but we want the absolute value
	l1_distance_layer = Lambda(
		lambda tensors: K.abs(tensors[0] - tensors[1]))
	l1_distance = l1_distance_layer([encoded_image_1, encoded_image_2])
	
	print("added l1")
	
	# Same class or not prediction
	prediction = Dense(units=1, activation='relu')(l1_distance)
	model = Model(inputs=[input_image_1, input_image_2], outputs=prediction)
	
	print("Created model")
	
	#optimizer = SGD(learning_rate=0.01, momentum=0.0001, nesterov=False)
	optimizer = Adam(learning_rate=0.00005)
	
	model.compile(loss='binary_crossentropy', metrics=['binary_accuracy'], optimizer=optimizer)
	
	print("compiled model")

batch_size = 800
batch_manager = BatchManager(batch_size)
batches = batch_manager.max_batches

print("Got the batches")

for i in range(batches):
	#print(batches[i][0])
	batch = batch_manager.get_next_batch()
	train_loss, train_accuracy = model.train_on_batch(batch[0], batch[1])
	print("-------" + str(i) + "/" + str(batches) + "(" + str(i/batches*100) + "%)--------")
	if(i % 50 == 0):
		model.save_weights('checkpoints/checkpoint')
	print(train_loss)
	print(train_accuracy)

model.save_weights("speech_diff_model.h5")
